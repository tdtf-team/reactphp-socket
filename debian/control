Source: reactphp-socket
Section: php
Priority: optional
Maintainer: Teckids Debian Task Force <tdtf@lists.teckids.org>
Uploaders: Dominik George <natureshadow@debian.org>,
 Thorsten Glaser <tg@mirbsd.de>
Build-Depends:
 debhelper-compat (= 13),
 pkg-php-tools (>= 1.7~)
Standards-Version: 4.5.0
Rules-Requires-Root: no
Homepage: https://github.com/reactphp/socket
Vcs-Git: https://salsa.debian.org/tdtf-team/reactphp-socket.git
Vcs-Browser: https://salsa.debian.org/tdtf-team/reactphp-socket

Package: php-react-socket
Architecture: all
Depends: ${misc:Depends}, ${phpcomposer:Debian-require}
Suggests: ${phpcomposer:Debian-suggest}
Replaces: ${phpcomposer:Debian-replace}
Breaks: ${phpcomposer:Debian-conflict}, ${phpcomposer:Debian-replace}
Provides: ${phpcomposer:Debian-provide}
Description: Asynchronous client and server socket connections for ReactPHP
 This library implements asynchronous TCP/IP client and server connections for
 ReactPHP, both plaintext and TLS-secured.
 .
 The socket library provides re-usable interfaces for a socket-layer server
 and client based on the EventLoop and Stream components. Its server component
 allows one to build networking servers that accept incoming connections from
 networking clients (such as an HTTP server). Its client component allows one
 to build networking clients that establish outgoing connections to
 networking servers (such as an HTTP or database client). This library
 provides async, streaming means for all of this, so you can handle multiple
 concurrent connections without blocking.
